package main

import (
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/lucassimon/go-rest-tutorial/controllers"
	"gopkg.in/mgo.v2"
)

func getSession() *mgo.Session {
	s, err := mgo.Dial("mongodb://localhost/airport")

	if err != nil {
		panic(err)
	}

	return s
}

func main() {
	r := httprouter.New()

	uc := controllers.NewUserController(getSession())

	r.POST("/users/", uc.CreateUser)

	r.GET("/users/", uc.GetUsers)

	r.GET("/users/:id", uc.GetUser)

	r.DELETE("/users/:id", uc.RemoveUser)

	r.GET("/", func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

		fmt.Fprint(w, "Welcome!")
	})

	http.ListenAndServe("localhost:3000", r)
}
