package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type (
	User struct {
		Id       bson.ObjectId `json:"id" bson:id,omitempty`
		_id      bson.ObjectId `json:"_id" bson:_id,omitempty`
		Name     string        `json:"name" bson:"name"`
		Gender   string        `json:"gender" bson:"gender" `
		Age      int           `json:"age" bson:"age"`
		Password string        `json:"secret,omitempty" bson:"secret"`
		created  time.Time
	}
)
